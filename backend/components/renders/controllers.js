var PublicationsServices = require('../publications/services');
var authenticationsServices = require('../auth/services');

exports.getHomeInfo = function(req,res,next){
    PublicationsServices.getPublications({})
	.then(function(result){
		let data = {
            modalites: require('../../static/modalites.json'),
            subcategories: require('../../static/subcategories.json'),
            brands: require('../../static/brands.json'),
            products: result
        };
        console.log(data.products);
        console.log(data.products[0].images);

        res.render('home', {layout: 'default', template: 'home-one', data});
	})
	.catch(function(err){
		res.send(err);
	})
        	
};

exports.publicationDetail = function(req,res,next){
    const { id } = req.params;
    PublicationsServices.publicationDetail(id)
	.then(function(result){
		let data = {
            modalites: require('../../static/modalites.json'),
            subcategories: require('../../static/subcategories.json'),
            brands: require('../../static/brands.json'),
            result
        };
        res.render('pages/product/product-detail', {layout: 'default', template: 'home-one', data});
	})
	.catch(function(err){
		res.send(err);
	})
        	
};

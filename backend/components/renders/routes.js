'use strict'
var express = require('express');
var api = express.Router();
var controller = require('./controllers');

api.get('/', controller.getHomeInfo);
api.get('/busqeda/:city/:type/:brand/:subcategory/:categoory/:status', controller.publicationDetail);
api.get('/detalle-publicacion/:id', controller.publicationDetail);
api.get('/usuario/iniciar-sesion/', controller.publicationDetail);
api.get('/usuario/registrarse/', controller.publicationDetail);
api.get('/mi-perfil', controller.publicationDetail);
api.get('/mi-perfil/mis-publicaciones', controller.publicationDetail);
api.get('/mi-perfil/crear-publicaciones', controller.publicationDetail);
api.get('/mi-perfil/mis-busquedas', controller.publicationDetail);
api.get('/legal/terminos', controller.publicationDetail);


module.exports = api;


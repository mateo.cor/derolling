'use strict'
var express = require('express');
var api = express.Router();
var Controller = require('./controllers');
var userMiddleware = require('../../middlewares/auth');


api.post('/publications/new-publication',userMiddleware.isAuth,Controller.newPublication);
api.post('/publications/add-image/:id',userMiddleware.isAuth,Controller.addImage);
api.post('/publications/update-publication/:id',userMiddleware.isAuth,Controller.updatePublication);
api.get('/publications/my-publications',userMiddleware.isAuth,Controller.myPublications);
api.post('/publications/get-publications',Controller.getPublications);
api.get('/publications/publication-detail/:id',userMiddleware.isAuth,Controller.publicationDetail);
api.get('/publications/publication-detail-public/:id',Controller.publicationDetail);
api.get('/publications/categories/get-modalites',Controller.getModalites);
api.get('/publications/categories/get-subcategories',Controller.getSubcategories);
api.get('/publications/categories/get-brands',Controller.getBrands);

module.exports = api;
var Publication = require('./model').Publication;
var PublicationImage = require('./model').PublicationImage;
var service = {};
var moment = require('moment');
var bcrypt = require('bcrypt-nodejs');
const AWS = require('aws-sdk');
const spacesEndpoint = new AWS.Endpoint('sfo2.digitaloceanspaces.com');
// const ACCESSKEY = 'HMT6SZL4V2M3AXXQFLYX';
// const SECRETKEY = '4jjn4aMo64CSRuGLCiOV/0PiNyD9xTAuDbuJRHTPkmQ';
const ACCESSKEY = 'JQOFIYRAFUZ3GO6CY4VZ';
const SECRETKEY = '9VbXX3OJBjwsOIn7hFNAOGbpi7p5g7Js9MIi+AD+5l4';
var service = require('../../services/auth');
var fs = require('fs');
service.newPublication = function(userid,data){
	return new Promise(function(resolve, reject){
    var publicacion = new Publication(data);
    publicacion.user = userid;
    publicacion.location = {
   type: "Point",
   coordinates: [36.098948, -112.110492]
  };
		Publication.create(publicacion,function(err,publicationCreated){
        if(err){
          return reject(err);
        }
        else{
          return  resolve(publicationCreated);
        }
      })
	})
}


service.updatePublication= function(publicationId ,data){
	return new Promise(function(resolve, reject){
		Publication.findByIdAndUpdate(publicationId,data, {new:true},function(err,publicationEdited){
        if(err){
          return reject(err);
        }
        else{
          return  resolve(publicationEdited);
        }
      })
	})
}
service.addImage = function(id, publicationId,data){
    return new Promise(function(resolve, reject){
      if (!data.files){
        return reject({message:"No hay imagenes"});
      }
      else{
        let file = data.files.image;
        

      let s3bucket = new AWS.S3({
        endpoint: spacesEndpoint,
        accessKeyId: ACCESSKEY,
        secretAccessKey: SECRETKEY
      });
      let imagekey = 'publications/'+id+'/'+moment()+'-'+file.name;
        var params = {
          Body: file.data,
          Bucket: "files-derolling",
          ACL: 'public-read',
          Key: imagekey
      };
      console.log('________________________')
        console.log(params)
      s3bucket.putObject(params, function(err, data) {
          if (err) {
            console.log(err, err.stack);
          }
          else{
            console.log('_____DATA___________________')
            console.log(data);
            let dataImage = {
              url:imagekey,
              publication: publicationId
            }
            PublicationImage.create(dataImage,function(err,imageAdded){
              if(err){
                return reject(err);
              }
              else{
                
                Publication.findByIdAndUpdate(publicationId,{ $push: { images: imageAdded._id  } },{new:true})
                .populate("images")
                .exec(function(error,result){
                    if(err){
                        return reject(error);
                    }
                    else if (result){
                        return resolve(result);
                    }
                })
              }
            })

          }     
      });
      }

    })
}

service.myPublications = function(userid){
  return new Promise(function(resolve, reject){

    Publication.find({user:userid})
  .populate("images")
.exec(function(err,publications){
        if(err){
          return reject(err);
        }
        else{
          return  resolve(publications);
        }
    });
  
  })
}
service.getPublications = function(data,latitude,longitude){
  return new Promise(function(resolve, reject){
    data.isactive=true;
      Publication.find(data)
  .populate("images")
.exec(function(err,publications){
        if(err){
          return reject(err);
        }
        else{
          return  resolve(publications);
        }
    });
  })
}

service.publicationDetail = function(id){
  return new Promise(function(resolve, reject){

    Publication.findById(id)
  .populate("images")
.exec(function(err,publications){
        if(err){
          return reject(err);
        }
        else{
          return  resolve(publications);
        }
    });
  
  })
}


service.removeImage = function(id){
  return new Promise(function(resolve, reject) {
    PublicationImage.findById(id)
          .exec(function (err, list) {
              if (err) {
                  return reject(err);
              }
              else {
                  if (!list){
                      return resolve({code:"404",message:"Recurso no encontrado"});
                  }
                  else{

                      fs.stat( './uploads/'+id+'.jpg', function (err, stats) {
                          if (err) {
                              return console.error(err);
                          }
                          else{
                            fs.unlink('./uploads/'+id+'.jpg',function(err){
                              if(err){
                                return reject(err);
                              }
                              else{
                               PublicationImage.remove({ _id: id }, function(err) {
                                 if (!err) {
                                     return resolve({code:"100",message:"Recurso eliminado"});
                                 }
                                 else {
                                     return reject(err)
                                 }
                             });
                              }
                           });  
                          }                        
                      });

                  }
              }
          });
  })
}


module.exports = service;


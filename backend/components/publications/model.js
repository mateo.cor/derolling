'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PublicationImageSchema =  Schema({
	url:{ type: String },
	publication:{ type: Schema.Types.ObjectId, ref: 'Publication' }
});
var PublicationImage = mongoose.model('PublicationImage',PublicationImageSchema);
module.exports.PublicationImage = PublicationImage;


var PublicationSchema =  Schema({
	user:{ type: Schema.Types.ObjectId, ref: 'User' },
	title:{ type:String },
	price:{ type:Number },
	images:[{ type:Schema.Types.ObjectId, ref:'PublicationImage'}],
	isactive:{ type:Boolean,default:false },
	description:{ type:String },
	city:{ type: String },
	state:{ type: String },
	country:{ type: String },
	category:{ type:String, default: '0' },
	brand:{ type:String },
	subcategory:{ type:String, default: '0' },
	size: { type:String, default: '0' },
	usestate:{ type:String, default: '0' },
	subcategorytype: { type:String, default: '0' },
	sleeve:{ type:String, default: '0' },
	lumenes:{ type:String, default: '0' },
});
var Publication = mongoose.model('Publication',PublicationSchema);
module.exports.Publication = Publication;
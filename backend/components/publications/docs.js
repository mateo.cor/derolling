/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */


 /**
 * @api {get} /api/{{APIVERSION}}/auth/checkemail/:email Check user email
 * @apiVersion 1.0.0
 * @apiName CheckEmail
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/checkemail/:email
 *
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 100,
 *       "is_social": false
 *     }
 */

 /**
 * @api {post} /api/{{APIVERSION}}/auth/resgister Startup user
 * @apiVersion 1.0.0
 * @apiName RegisterUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/register
 *
 * @apiParam (Request body) {String} username Full username.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * @apiParam (Request body) {String} id_type Type of identity document.
 * @apiParam (Request body) {Number} id_num Number of identification.
 * @apiParam (Request body) {Boolean} is_social Check if is Social login.
 * 
 */


  /**
 * @api {get} /api/{{APIVERSION}}/publications/get-publications/:city?/:category?/:price?/:sort? Get publications
 * @apiVersion 1.0.0
 * @apiName GetPublications
 * @apiGroup Publications
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/publications/get-publications/:city?/:category?/:price?/:sort?
 * 
 */
 
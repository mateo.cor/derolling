var Services = require('./services');


exports.newPublication = function(req,res,next){
	var userid = req.user.sub;
	var params = req.body;
	Services.newPublication(userid,params)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};

exports.addImage = function(req,res,next){
	var id = req.user.sub;
	var publicationId = req.params.id;
	var data = req;
	data.publication = publicationId;
	Services.addImage(id,publicationId ,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.updatePublication = function(req,res,next){
	var params = req.body;
	var publicationId = req.params.id;
	Services.updatePublication(publicationId,params)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.myPublications = function(req,res,next){
	var id = req.user.sub;
	Services.myPublications(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getPublications = function(req,res,next){
	var data = req.body;
	Services.getPublications(data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}

exports.publicationDetail = function(req,res,next){
	var id = req.params.id;
	Services.publicationDetail(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getModalites = function (req,res,next){
	var modalites = require('../../static/modalites.json');
	res.status(200).send(modalites);
}
exports.getSubcategories = function (req,res,next){
	var subcategories = require('../../static/subcategories.json');
	res.status(200).send(subcategories);
}
exports.getBrands = function (req,res,next){
	var brands = require('../../static/brands.json');
	res.status(200).send(brands);
}
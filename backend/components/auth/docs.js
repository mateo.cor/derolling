/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */


 /**
 * @api {get} /api/{{APIVERSION}}/auth/checkemail/:email Check user email
 * @apiVersion 1.0.0
 * @apiName CheckEmail
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/checkemail/:email
 *
 * 
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "code": 100,
 *       "is_social": false
 *     }
 */

 /**
 * @api {post} /api/{{APIVERSION}}/auth/resgister Startup user
 * @apiVersion 1.0.0
 * @apiName RegisterUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/register
 *
 * @apiParam (Request body) {String} username Full username.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * @apiParam (Request body) {String} id_type Type of identity document.
 * @apiParam (Request body) {Number} id_num Number of identification.
 * @apiParam (Request body) {Boolean} is_social Check if is Social login.
 * 
 */


  /**
 * @api {post} /api/{{APIVERSION}}/auth/login Login user
 * @apiVersion 1.0.0
 * @apiName LoginUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/login
 *
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * 
 */
   /**
 * @api {get} /api/{{APIVERSION}}/auth/me Get user data
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup Auth
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/me
 *
 * @apiHeader {String} Authorization User token.
 * 
 */

  /**
 * @api {put} /api/{{APIVERSION}}/auth/update-me Update user info
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup User
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/update-me
 *	
 * @apiHeader {String} Authorization User token.
 * @apiParam (Request body) {String} username Full username.
 * @apiParam (Request body) {String} email This email is required for login.
 * @apiParam (Request body) {String} password Password for login.
 * @apiParam (Request body) {String} id_type Type of identity document.
 * @apiParam (Request body) {Number} id_num Number of identification.
 * 
 */
   /**
 * @api {post} /api/{{APIVERSION}}/auth/set-avatar Set avatar
 * @apiVersion 1.0.0
 * @apiName SetAvatar
 * @apiGroup User
 * 
 * @apiSampleRequest http://localhost:5000/api/v1/auth/set-avatar
 *
 * @apiHeader {String} Authorization User token.
 * @apiParam (Request body) {File} avatar User avatar.
 * 
 */
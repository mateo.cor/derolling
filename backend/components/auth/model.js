'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema =  Schema({
	username:{ type:String, required:true },
	email:{ type:String, required:true },
	password:{ type:String, required:true },
	avatar:{ type:String, required:false },
	isactive:{ type:Boolean,default:true },
	socialnetwork:{type:String},
	phone:{type:Number},
	city:{ type: String },
	state:{ type: String },
	country:{ type: String },
	register_date:{ type:Date  },
	is_social:{type:Boolean}
});
var User = mongoose.model('User',UserSchema);
module.exports.User = User;



'use strict'
var express = require('express');
var api = express.Router();
var Controller = require('./controllers');
var userMiddleware = require('../../middlewares/auth');

//Autenticación

api.get('/auth/checkemail/:email',Controller.checkemail);
api.post('/auth/register',Controller.register);
api.post('/auth/login',Controller.login);
api.get('/auth/me',userMiddleware.isAuth,Controller.me);
api.post('/auth/update-me',userMiddleware.isAuth,Controller.updateme);
api.get('/auth/activate-me/:id',Controller.activateme);
api.post('/user/set-avatar',userMiddleware.isAuth,Controller.setAvatar);
api.get('/geo/all-cities/:country',Controller.getallCities);



module.exports = api;
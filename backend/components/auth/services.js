var User = require('./model').User;
var service = {};
var moment = require('moment');
var bcrypt = require('bcrypt-nodejs');
var service = require('../../services/auth');
service.startSuperAdmin = function(data){
	return new Promise(function(resolve, reject){
		User.findOne({nombre:data.nombre},function(err,result){
		if(err){
			console.log('Error 1 -------------')
			return reject(err);
		}
		else if (result){
			console.log('Error 2 -------------')
			return resolve(result);
		}
		else{
			console.log('No existe -------------')
			User.create(data,function(err,UserCreado){
				if(err){
					console.log('Error 3 -------------')
					return reject(err);
				}
				else{
					console.log('Error 4 -------------')
					return	resolve(UserCreado);
				}
			})
		}
	})
	})
}
service.checkemail = function(data){
  return new Promise(function(resolve, reject){
User.findOne({email:data.toLowerCase()}, function(err, user) {
        // Comprobar si hay errores
        // Si el User existe o no
        // Y si la contraseña es correcta
        if (err) {
            return reject({
                message: "Ocurrio un error al hacer la peticion",
                error:err
            });
        } else {
           if(user){
            if (!user.isactive) {
              return resolve({code:103,message:"La cuenta no esta activa"});
            }
            else{
              if (user.is_social) {
                return resolve({code:104,is_social:user.is_social});
              }
              else{
                return resolve({code:100,is_social:user.is_social});
              }
            }
           }
             else{
                    return reject({code:101,message:"El usuaio no existe"});
                }
        }

    });
  })
}
service.register = function (params){
	var currentDate = moment().unix();

   return new Promise(function(resolve, reject){
      var user = new User({
                username : params.username,
                email: params.email,
                password:params.password,
                id_type:params.id_type,
                id_num:params.id_num,
                city:params.city,
                lat:params.lat,
                long:params.long,
                state:params.state,
                country:params.country,
                register_date:currentDate,
                is_social:params.is_social
  });
      if (!params.avatar) {
        user.avatar="-";
      }
      else{
        user.avatar=params.avatar;
      }
     bcrypt.hash(params.password,null,null,function(err,hash){
        if(err){
            console.log(err)
        }
        else{
            user.password = hash;
        }
    })
        
    User.findOne({email:params.email},(err,userfinded)=>{
        if (userfinded) {
           return resolve({code:404,message:"Este User ya existe"});
        }
        else{
            User.create(user,function(err,usercreated) {
          if (err) {
              return reject({code:501,data:{message:"Error al crear el usuario", err: err}});
          }
          else{
            var token = service.createToken(usercreated);
            
              return resolve({code:100,message:"User creado con exito!",token:token});
          }
    })
        }
    })

    
   });
	
}
service.login = function(data){
	return new Promise(function(resolve, reject){
User.findOne({email:data.email.toLowerCase()}, function(err, user) {
        // Comprobar si hay errores
        // Si el User existe o no
        // Y si la contraseña es correcta
        if (err) {
            return reject({
                message: "Petición denegada, correo o contraseña invalidos"
            });
        } else {
           if(user){
            console.log(data.password);
            console.log(user.password);
             bcrypt.compare(data.password,user.password,(err,check)=>{
                if(check){
                        return resolve({code:100,token:service.createToken(user)});
                }
                else{
                  console.log(err);
                   return reject({code:404,message:"El usuaio no pudo logearse correctamente"});
                }
            })
           }
             else{
                    return reject({code:404,message:"El usuaio no pudo logearse correctamente"});
                }
        }

    });
	})
}

service.setAvatar = function(id,data){
    return new Promise(function(resolve, reject){
        
         if (!data.files){
                return reject({message:"No hay imagenes"});
         }
         else{
            // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
              let avatar = data.files.avatar;
             console.log(id);
              // Use the mv() method to place the file somewhere on your server
              avatar.mv('./uploads/avatars/'+id+'.jpg', function(err,res) {
                    if(err){
                        return reject(err);
                    }
                    else {
                        var localdata = {
                            avatar: process.env.IMAGESHOST+'/avatars/'+id+'.jpg'
                        }
                        User.findByIdAndUpdate(id,localdata,{new:true},function(error,result){
                        if(err){
                            return reject(error);
                        }
                        else if (result){
                            return resolve(result);
                        }
                    })
                    }
              });
         }

    })
}


service.me = function(id){
    return new Promise(function(resolve, reject){
    User.findById(id, function(err, list) {
        if (err) {
             return reject(err);
        }
        else{
          if (list) {
            return resolve(list);
          }
            else{
              return reject({message:"el usuario no existe",code:404});
            }
        }
    });
    })
}
service.updateme =  function(id,data){
    return new Promise(function(resolve, reject){
    User.findByIdAndUpdate(id,data,{new:true}, function(err, updated) {
        if (err) {
             return reject(err);
        }
        else{
          var token = service.createToken(updated);   
          return resolve({code:100,message:"User actualizado con exito!",token:token,updated:updated});
        }
    });
    })
}


module.exports = service;


var Services = require('./services');
var User = require('./model').User;
var bcrypt = require('bcrypt-nodejs');


exports.register = function(req,res,next){
	var params = req.body;

   Services.register(params)
	.then(function(user){
	if (user.code==404) {
			res.send(user);
	}
	else{
		  res.status(200).send(user);
// 		user.data.user.name = user.data.user.username;
// 		loadTemplate('registro', [user.data.user]).then((results) => {
//     return Promise.all(results.map((result) => {
//         sendEmail({
//             to: result.context.email,
//             from: 'Terridata :)',
//             subject: result.email.subject,
//             html: result.email.html,
//             text: result.email.text,
//         });
//     }));
// }).then((response) => {
// 	console.log(user);
//     res.status(200).send(user);
// }).catch((err)=>{
//     res.status(200).send({code:501,data:{message:"Problemas al crear la cuenta",err:err}});
// })
	}

		
	})
	.catch(function(err){
		res.status(500).send(err)
	})
};
exports.login = function(req,res,next){
   Services.login(req.body)
	.then(function(result){

		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
}



const nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'apps.derolling@gmail.com',
            pass: 'apps.derolling',
        },
    }),
    EmailTemplate = require('email-templates').EmailTemplate,
    path = require('path'),
    Promise = require('bluebird');

// I changed the emails from what's in the tutorial because people kept using
// info@geeklaunch.net and sending me their test emails. :P Lesson learned. :)
//
// So yeah, change the emails below from 'example@example.tld' to YOUR email,
// please.
//
// Thank you!

function sendEmail (obj) {
    return transporter.sendMail(obj);
}

function loadTemplate (templateName, contexts) {
 let template = new EmailTemplate(path.join(__dirname, '../../templates', templateName));
    return Promise.all(contexts.map((context) => {
        return new Promise((resolve, reject) => {
            template.render(context, (err, result) => {
                if (err) reject(err);
                else resolve({
                    email: result,
                    context,
                });
            });
        });
    }));
}

exports.setAvatar = function(req,res,next){
	var id = req.user.sub;
	console.log(id);
	var data = req;
	Services.setAvatar(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.me = function(req,res,next){
	var id = req.user.sub;
	Services.me(id)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.updateme = function(req,res,next){
	var id = req.user.sub;
	var data = req.body;
	Services.updateme(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.activateme = function(req,res,next){
	var id = req.params.id;
	var data = {
		isactive: true
	};
	Services.updateme(id,data)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};

exports.checkemail = function(req,res,next){
	var email = req.params.email;
	Services.checkemail(email)
	.then(function(result){
		res.send(result);
	})
	.catch(function(err){
		res.send(err);
	})
	
};
exports.getallCities = function(req,res,next){
	var country = req.params.country;
	var list = require('../../static/geo/'+country+'.json');
	res.status(200).send(list);
	
};
module.exports = {  
    options: {
        reconnectTries: process.env.RECONECTTIRES,
        reconnectInterval: process.env.RECONNECTINTERVAL,
        socketTimeoutMS: process.env.SOCKETTIMEOUT,
        connectTimeoutMS: process.env.CONNECTTIMEOUT,
        useNewUrlParser: process.env.NEWURLPARSER,
        autoIndex: process.env.AUTOINDEX,
    },
    dbpath: 'mongodb://localhost:27017/'+ process.env.DATABASE
  };
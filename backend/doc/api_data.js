define({ "api": [
  {
    "type": "get",
    "url": "/api/{{APIVERSION}}/auth/get-access-token",
    "title": "GEt Access token",
    "version": "1.0.0",
    "name": "CheckEmail",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://34.215.177.171:5000/api/v1/auth/get-access-token"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "territoken",
            "description": "<p>Token in SHA1.</p>"
          }
        ]
      }
    },
    "filename": "./components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/api/{{APIVERSION}}/auth/checkemail/:email",
    "title": "Check user email",
    "version": "1.0.0",
    "name": "CheckEmail",
    "group": "Auth",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x_td_auth_token",
            "description": "<p>Token authorization.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://34.215.177.171:5000/api/v1/auth/checkemail/:email"
      }
    ],
    "filename": "./components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/api/{{APIVERSION}}/auth/me",
    "title": "Get user data",
    "version": "1.0.0",
    "name": "GetUser",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://34.215.177.171:5000/api/v1/auth/me"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x_td_auth_token",
            "description": "<p>Token authorization.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "filename": "./components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/api/{{APIVERSION}}/auth/login",
    "title": "Login user",
    "version": "1.0.0",
    "name": "LoginUser",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://34.215.177.171:5000/api/v1/auth/login"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x_td_auth_token",
            "description": "<p>Token authorization.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>This email is required for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password for login.</p>"
          }
        ]
      }
    },
    "filename": "./components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/api/{{APIVERSION}}/auth/resgister",
    "title": "Startup user",
    "version": "1.0.0",
    "name": "RegisterUser",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://34.215.177.171:5000/api/v1/auth/register"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x_td_auth_token",
            "description": "<p>Token authorization.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Full username.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>This email is required for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "id_type",
            "description": "<p>Type of identity document.</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": false,
            "field": "id_num",
            "description": "<p>Number of identification.</p>"
          }
        ]
      }
    },
    "filename": "./components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc/main.js",
    "group": "C__Users_Usuario_Documents_terridata_apidoc_main_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_apidoc_main_js",
    "name": ""
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ajv/dist/ajv.bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ajv_dist_ajv_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/aws-sign2/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_aws_sign2_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/read.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_read_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_read_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/json.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_json_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_json_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/raw.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_raw_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_raw_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/text.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_text_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_text_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/text.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_text_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_text_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/urlencoded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_urlencoded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_urlencoded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/urlencoded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_urlencoded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_urlencoded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/urlencoded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_urlencoded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_body_parser_lib_types_urlencoded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/braces/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_braces_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/browser_build/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/browser_build/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/browser_build/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/browser_build/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/browser_build/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_browser_build_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/lib/bson/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/lib/bson/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/lib/bson/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/lib/bson/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/bson/lib/bson/bson.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_bson_lib_bson_bson_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/busboy/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/busboy/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/busboy/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/busboy/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/busboy/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/busboy/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_busboy_node_modules_isarray_build_build_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cache-base/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cache_base_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/center-align/node_modules/lazy-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_center_align_node_modules_lazy_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_center_align_node_modules_lazy_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cheerio/lib/api/css.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cheerio/lib/api/css.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cheerio/lib/api/css.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cheerio/lib/api/css.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cheerio/lib/api/css.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cheerio_lib_api_css_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/class-utils/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_class_utils_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/co/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_co_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/commander/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_commander_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/component-emitter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/component-emitter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/component-emitter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/component-emitter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/component-emitter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/component-emitter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/component-emitter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_component_emitter_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/consolidate/lib/consolidate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/consolidate/lib/consolidate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/consolidate/lib/consolidate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/consolidate/lib/consolidate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_consolidate_lib_consolidate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_content_disposition_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cookie-signature/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cookie_signature_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cookie_signature_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cookie-signature/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_cookie_signature_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_cookie_signature_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/copy-descriptor/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_copy_descriptor_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_copy_descriptor_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/node.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_node_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_node_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/node.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_node_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_node_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/src/node.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_node_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_debug_src_node_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/dicer/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/dicer/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/dicer/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/dicer/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/dicer/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/dicer/node_modules/isarray/build/build.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_dicer_node_modules_isarray_build_build_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/etag/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_etag_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_etag_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/expand-brackets/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/expand-brackets/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/expand-brackets/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/expand-brackets/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/expand-brackets/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/expand-brackets/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_expand_brackets_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/express.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_express_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_express_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/middleware/init.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_middleware_init_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_middleware_init_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/middleware/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_middleware_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_middleware_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/layer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_layer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_layer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/layer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_layer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_layer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/layer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_layer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_layer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/route.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_route_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_router_route_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/extglob/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_extglob_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/fragment-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/fragment-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/fragment-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/fragment-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/fragment-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_fragment_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/property.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_property_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_property_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/property.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_property_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_property_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/property.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_property_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_property_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private.",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/selector.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/selector.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/selector.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/selector.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_selector_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_lib_utils_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private.",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/juice/tmp/bundle.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_juice_tmp_bundle_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/jwt-simple/lib/jwt.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_jwt_simple_lib_jwt_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_jwt_simple_lib_jwt_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/jwt-simple/lib/jwt.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_jwt_simple_lib_jwt_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_jwt_simple_lib_jwt_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/lazy-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_lazy_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_lazy_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/map-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/map-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/map-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/map-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/map-cache/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_map_cache_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_media_typer_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/micromatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_micromatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose-legacy-pluralize/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_legacy_pluralize_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_legacy_pluralize_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/aggregate.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_aggregate_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browserDocument.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browserDocument_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browserDocument_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cast.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cast_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cast_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_collection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/AggregationCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/AggregationCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/AggregationCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/AggregationCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/AggregationCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/AggregationCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_AggregationCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/QueryCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/QueryCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/QueryCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/QueryCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/QueryCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/cursor/QueryCursor.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_cursor_QueryCursor_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document_provider.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_provider_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_provider_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/document_provider.web.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_provider_web_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_document_provider_web_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/collection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_collection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/drivers/node-mongodb-native/connection.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_drivers_node_mongodb_native_connection_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/cast.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_cast_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_cast_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/disconnected.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_disconnected_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_disconnected_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/messages.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_messages_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_messages_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/objectExpected.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_objectExpected_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_objectExpected_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/objectParameter.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_objectParameter_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_objectParameter_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/strict.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_strict_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_strict_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/validation.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_validation_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_validation_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/validator.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_validator_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_validator_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/error/version.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_version_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_error_version_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/model.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_model_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/promise_provider.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_promise_provider_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_promise_provider_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/promise_provider.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_promise_provider_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_promise_provider_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/promise_provider.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_promise_provider_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_promise_provider_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/query.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/boolean.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/boolean.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/boolean.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/boolean.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/boolean.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_boolean_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/date.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_date_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/decimal128.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/decimal128.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/decimal128.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/decimal128.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_decimal128_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_documentarray_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/mixed.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/mixed.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/mixed.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/mixed.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_mixed_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/number.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/number.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/number.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/number.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/number.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/number.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/number.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_number_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/objectid.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/objectid.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/objectid.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/objectid.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/objectid.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/objectid.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_objectid_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schema/string.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schema_string_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/schematype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_schematype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/services/cursor/eachAsync.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_services_cursor_eachAsync_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_services_cursor_eachAsync_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/services/setDefaultsOnInsert.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_services_setDefaultsOnInsert_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_services_setDefaultsOnInsert_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/services/updateValidators.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_services_updateValidators_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_services_updateValidators_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/array.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_array_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/buffer.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_buffer_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/documentarray.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_documentarray_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/embedded.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_embedded_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/subdocument.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/subdocument.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/subdocument.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/types/subdocument.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_types_subdocument_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/virtualtype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/virtualtype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/virtualtype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/virtualtype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mongoose/lib/virtualtype.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mongoose_lib_virtualtype_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/mquery.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_mquery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/lib/utils.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/mquery/node_modules/sliced/lib/sliced.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_mquery_node_modules_sliced_lib_sliced_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_mquery_node_modules_sliced_lib_sliced_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_ms_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nanomatch/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nanomatch_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/debug.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/node.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_node_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_node_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/node.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_node_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_node_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/nodemon/node_modules/debug/src/node.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_node_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_nodemon_node_modules_debug_src_node_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/object-copy/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_object_copy_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_object_copy_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/path-to-regexp/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_path_to_regexp_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_path_to_regexp_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/regex-not/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_regex_not_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_regex_not_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/regex-not/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_regex_not_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_regex_not_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/repeat-string/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_repeat_string_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_repeat_string_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/set-getter/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_set_getter_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_set_getter_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/sliced/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_sliced_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_sliced_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/compiler.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_compiler_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_compiler_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/parser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_parser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon/lib/source-maps.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_source_maps_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_lib_source_maps_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-node/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_node_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/snapdragon-util/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_snapdragon_util_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/static-extend/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_static_extend_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_static_extend_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/to-regex/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_to_regex_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_to_regex_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/to-regex/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_to_regex_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_to_regex_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/use/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_use_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_use_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/use/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_use_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_use_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/util-deprecate/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_util_deprecate_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_util_deprecate_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/util-deprecate/browser.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_util_deprecate_browser_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_util_deprecate_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/utils-merge/index.js",
    "group": "C__Users_Usuario_Documents_terridata_node_modules_utils_merge_index_js",
    "groupTitle": "C__Users_Usuario_Documents_terridata_node_modules_utils_merge_index_js",
    "name": "Public"
  },
  {
    "type": "post",
    "url": "/api/{{APIVERSION}}/auth/set-avatar",
    "title": "Set avatar",
    "version": "1.0.0",
    "name": "SetAvatar",
    "group": "User",
    "sampleRequest": [
      {
        "url": "http://34.215.177.171:5000/api/v1/auth/set-avatar"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "File",
            "optional": false,
            "field": "avatar",
            "description": "<p>User avatar.</p>"
          }
        ]
      }
    },
    "filename": "./components/auth/docs.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/api/{{APIVERSION}}/auth/update-me",
    "title": "Update user info",
    "version": "1.0.0",
    "name": "UpdateUser",
    "group": "User",
    "sampleRequest": [
      {
        "url": "http://34.215.177.171:5000/api/v1/auth/update-me"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x_td_auth_token",
            "description": "<p>Token authorization.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Full username.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>This email is required for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "id_type",
            "description": "<p>Type of identity document.</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": false,
            "field": "id_num",
            "description": "<p>Number of identification.</p>"
          }
        ]
      }
    },
    "filename": "./components/auth/docs.js",
    "groupTitle": "User"
  }
] });

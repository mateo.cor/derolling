'use strict'
const app = require('./app'),
      https = require('https'),
      express = require('express'),
      mongoose = require('mongoose'),
      fs = require('fs'),
      path = require('path'),
      chalk = require('chalk'),
      connected = chalk.bold.green,
      log = console.log,
      { ssl_port, ssl_connection, non_ssl_port } = require('./config/server-connection'), 
      { options, dbpath } = require('./config/db-connection');


// mongoose.connect(dbpath, options)
    
// mongoose.connection.on('connected', function(){
//     log(connected(`Mongoose connection is open ${dbpath}`))
//     console.log('Successfully connected to mongodb database');
//     https.createServer({
//         port: ssl_port,
//         key: fs.readFileSync(ssl_connection.key),
//         cert: fs.readFileSync(ssl_connection.cert),
//         ca: [fs.readFileSync(ssl_connection.ca1),fs.readFileSync(ssl_connection.ca2),fs.readFileSync(ssl_connection.ca3)]
//     }, app).listen(ssl_port, function(err, nonerr){
//         if(err){
//             log(err);
//         }
//         else{
//             log(nonerr);
//         }
//         console.log(`Derolling is now running on https ${ssl_port}...`);
//         app.use(express.static(path.join(__dirname, '../public')));
//         app.use('/*',express.static(path.join(__dirname, '../public')));
//     }); 
// })

// mongoose.connection.on('error', function(err){
//     log(`Connection ERROR ${err}`)
//     log(error(`Mongoose connection has occured ${err} error`))
// })

// mongoose.connection.on('disconnected', function(){
//     log(disconnected(`Mongoose is disconnected`))
// })

// process.on('SIGINT', function(){
//     mongoose.connection.close(function(){
//         log(`Connection terminate`)
//         log(termination(`Mongoose connection is disconnected due to application termination`))
//         process.exit(0)
//     })
// })

mongoose.connect(dbpath, options)
    
mongoose.connection.on('connected', function(){
    log(connected(`Mongoose connection is open ${dbpath}`))
    console.log('Successfully connected to mongodb database');
    app.listen(non_ssl_port, function(err, nonerr){
        if(err){
            log(err);
        }
        else{
            log(nonerr);
        }
        console.log(`Derolling is now running on https ${non_ssl_port}...`);
        app.use(express.static(path.join(__dirname, '../public')));
        app.use('/*',express.static(path.join(__dirname, '../public')));
    }); 
})

mongoose.connection.on('error', function(err){
    log(`Connection ERROR ${err}`)
    log(error(`Mongoose connection has occured ${err} error`))
})

mongoose.connection.on('disconnected', function(){
    log(disconnected(`Mongoose is disconnected`))
})

process.on('SIGINT', function(){
    mongoose.connection.close(function(){
        log(`Connection terminate`)
        log(termination(`Mongoose connection is disconnected due to application termination`))
        process.exit(0)
    })
})
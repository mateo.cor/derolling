'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = require('../config/token').TOKEN_SECRET;
var secretAccess = require('../config/token').TOKEN_SECRET_ACCESS;
var CryptoJS = require("crypto-js");
var CryptoJSsHA1 = require("crypto-js/sha1");
var service = require('../services/auth');
var http = require('http');
var querystring = require('querystring');

exports.isAuth = function (req,res,next) {
	if (!req.headers.authorization) {
		return res.status(403).send({message:"No estas logeado"});
	}
	var token = req.headers.authorization.replace(/['"]+/g,'');
	try{
		var payload = jwt.decode(token,secret);
		if (payload.exp <= moment().unix()) {
			return res.status(401).send({message:"El token ha caducado"});
		}
	}catch(ex){
		return res.status(401).send({message:"El token no es valido"});
	}
	req.user = payload;
	// if(!req.user.isactive){
	// 	return res.status(403).send({message:"El usuario no ha sido activado"});
	// }
	next();
}

define({ "api": [
  {
    "type": "get",
    "url": "/api/{{APIVERSION}}/auth/checkemail/:email",
    "title": "Check user email",
    "version": "1.0.0",
    "name": "CheckEmail",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/checkemail/:email"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 100,\n  \"is_social\": false\n}",
          "type": "json"
        }
      ]
    },
    "filename": "components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/api/{{APIVERSION}}/auth/checkemail/:email",
    "title": "Check user email",
    "version": "1.0.0",
    "name": "CheckEmail",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/checkemail/:email"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 100,\n  \"is_social\": false\n}",
          "type": "json"
        }
      ]
    },
    "filename": "components/publications/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/api/{{APIVERSION}}/auth/me",
    "title": "Get user data",
    "version": "1.0.0",
    "name": "GetUser",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/me"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "filename": "components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/api/{{APIVERSION}}/auth/login",
    "title": "Login user",
    "version": "1.0.0",
    "name": "LoginUser",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/login"
      }
    ],
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>This email is required for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password for login.</p>"
          }
        ]
      }
    },
    "filename": "components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/api/{{APIVERSION}}/auth/resgister",
    "title": "Startup user",
    "version": "1.0.0",
    "name": "RegisterUser",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/register"
      }
    ],
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Full username.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>This email is required for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "id_type",
            "description": "<p>Type of identity document.</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": false,
            "field": "id_num",
            "description": "<p>Number of identification.</p>"
          },
          {
            "group": "Request body",
            "type": "Boolean",
            "optional": false,
            "field": "is_social",
            "description": "<p>Check if is Social login.</p>"
          }
        ]
      }
    },
    "filename": "components/auth/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/api/{{APIVERSION}}/auth/resgister",
    "title": "Startup user",
    "version": "1.0.0",
    "name": "RegisterUser",
    "group": "Auth",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/register"
      }
    ],
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Full username.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>This email is required for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "id_type",
            "description": "<p>Type of identity document.</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": false,
            "field": "id_num",
            "description": "<p>Number of identification.</p>"
          },
          {
            "group": "Request body",
            "type": "Boolean",
            "optional": false,
            "field": "is_social",
            "description": "<p>Check if is Social login.</p>"
          }
        ]
      }
    },
    "filename": "components/publications/docs.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/api/{{APIVERSION}}/publications/get-publications/:city?/:category?/:price?/:sort?",
    "title": "Get publications",
    "version": "1.0.0",
    "name": "GetPublications",
    "group": "Publications",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/publications/get-publications/:city?/:category?/:price?/:sort?"
      }
    ],
    "filename": "components/publications/docs.js",
    "groupTitle": "Publications"
  },
  {
    "type": "post",
    "url": "/api/{{APIVERSION}}/auth/set-avatar",
    "title": "Set avatar",
    "version": "1.0.0",
    "name": "SetAvatar",
    "group": "User",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/set-avatar"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "File",
            "optional": false,
            "field": "avatar",
            "description": "<p>User avatar.</p>"
          }
        ]
      }
    },
    "filename": "components/auth/docs.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/api/{{APIVERSION}}/auth/update-me",
    "title": "Update user info",
    "version": "1.0.0",
    "name": "UpdateUser",
    "group": "User",
    "sampleRequest": [
      {
        "url": "http://localhost:5000/api/v1/auth/update-me"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Full username.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>This email is required for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password for login.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "id_type",
            "description": "<p>Type of identity document.</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": false,
            "field": "id_num",
            "description": "<p>Number of identification.</p>"
          }
        ]
      }
    },
    "filename": "components/auth/docs.js",
    "groupTitle": "User"
  }
] });

'use strict'
const app = require('./app'),
	  express = require('express'),
	  router = express.Router(),
	  mongoose = require('mongoose'),
	  hbs = require( 'express-handlebars'),
      fs = require('fs'),
      path = require('path'),
      chalk = require('chalk'),
	  connected = chalk.bold.green,
	  server = require('http').createServer(app),
      log = console.log,
      { non_ssl_port, enviroment } = require('./config/server-connection'), 
      { options, dbpath } = require('./config/db-connection');
	  app.set('view engine', 'hbs');
	  app.engine( 'hbs', hbs( {
		extname: 'hbs',
		defaultView: 'default',
		layoutsDir: __dirname + '/views/pages/',
		partialsDir: __dirname + '/views/partials/'
	  }));
if(enviroment === 'dev'){
	console.log('SIP')
	mongoose.connect(dbpath, options)
    
	mongoose.connection.on('connected', function(){
		log(connected(`Mongoose connection is open ${dbpath}`))
		console.log('Successfully connected to mongodb database');
		app.listen(non_ssl_port, () => {
			console.log("Aplicación inicializada, bienvenido a Derolling!! " + non_ssl_port);
			
			app.use(express.static(path.join(__dirname, '../publicv1')));
			app.use('/publicv1/*',express.static(path.join(__dirname, '../publicv1')));
			app.use(express.static(path.join(__dirname, 'docs/')));
			app.use(express.static(path.join(__dirname, 'uploads/')));
		});      
	})
	
	mongoose.connection.on('error', function(err){
		log(`Connection ERROR ${err}`)
		log(error(`Mongoose connection has occured ${err} error`))
	})
	
	mongoose.connection.on('disconnected', function(){
		log(disconnected(`Mongoose is disconnected`))
	})
	
	process.on('SIGINT', function(){
		mongoose.connection.close(function(){
			log(`Connection terminate`)
			log(termination(`Mongoose connection is disconnected due to application termination`))
			process.exit(0)
		})
	})
}
else{
	router.get('*',function(req,res){
		res.redirect('https://' + req.headers.host + req.url);
	});
	app.use("/",router);
	server.listen(non_ssl_port,function(){
		console.log("Live at Port "+non_ssl_port);
	});

}


